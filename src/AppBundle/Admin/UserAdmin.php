<?php
/**
 * Created by PhpStorm.
 * User: adam
 * Date: 11.06.2018
 * Time: 13:39
 */

namespace AppBundle\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class UserAdmin extends AbstractAdmin
{


    protected static function flattenRoles($rolesHierarchy)
    {
        $flatRoles = [];
        foreach ($rolesHierarchy as $key => $roles) {
            $flatRoles[$key] = $key;
            if (empty($roles)) {
                continue;
            }

            foreach ($roles as $role) {
                if (!isset($flatRoles[$role])) {
                    $flatRoles[$role] = $role;
                }
            }
        }

        return $flatRoles;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper->add('username', TextType::class)
            ->add('email', TextType::class)
            ->add('confirmationToken', TextType::class, array('required' => false))
            ->add('enabled', TextType::class);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('username')
            ->add('email')
            ->add('confirmationToken')
            ->add('enabled');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('username')
            ->addIdentifier('email')
            ->addIdentifier('confirmationToken')
            ->addIdentifier('enabled');
    }
}