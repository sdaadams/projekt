<?php

namespace AppBundle\Admin;


use Doctrine\DBAL\Types\IntegerType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class LakesAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('id', TextType::class)
            ->add('name', TextType::class)
            ->add('city', TextType::class)
            ->add('address', TextType::class)
            ->add('area', TextType::class)
            ->add('info', TextType::class);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id')
            ->add('name')
            ->add('city')
            ->add('address')
            ->add('area')
            ->add('info');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id')
            ->addIdentifier('name')
            ->addIdentifier('city')
            ->addIdentifier('address')
            ->addIdentifier('area')
            ->addIdentifier('info');
    }
}