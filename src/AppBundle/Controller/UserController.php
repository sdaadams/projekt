<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Entity\UserData;
use AppBundle\Form\UserDataForm;
use AppBundle\Libs\JsonResponse\ApiProblem;
use AppBundle\Libs\JsonResponse\ApiResponse;
use Beeflow\AjaxBundle\Utils\AjaxResponse;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use FOS\UserBundle\Form\Type\ProfileFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{

    /**
     * Lista uzytkownikow serwisu
     *
     * @return Response
     */
    public function indexAction(): Response
    {
        $user = $this->getUser();

        //jezeli user nie istnieje przekieruj go do homepage
        if (!($user instanceof User)) {
            return new RedirectResponse($this->generateUrl('homepage'));
        }

        if (!$this->isGranted('ROLE_ADMIN')) {
            return $this->render('@App/errors/403.html.twig', [
                'error_message' => 'Brak uprawnień'
            ]);
        }

        $users = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->findAll();
        $userData = $this->getDoctrine();

        return $this->render('@App/user/index.html.twig', [
            'users' => $users
        ]);
    }


    /**
     * @param Request $request
     * @param int     $userId
     *
     * @return Response
     */
    public function editAction(Request $request, int $userId): Response
    {
        $user = $this->getUser();

        if (!($user instanceof User)) {
            return new RedirectResponse($this->generateUrl('homepage'));
        }

        if (!$this->isGranted('ROLE_ADMIN') && $user->getId() != $userId) {
            return $this->render('@App/errors/403.html.twig', [
                'error_message' => 'Brak uprawnień'
            ]);
        }

        $userToEdit = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->find($userId);

//        if (!($userToEdit instanceof User)) {
//            throw new NotFoundHttpException('Nie ma takiego użytkownika');
//        }
        if (!($userToEdit instanceof User)) {
            return $this->render('@App/errors/404.html.twig', [
                'error_message' => 'Nie ma takiego użytkownika'
            ]);
        }

        $form = $this->createForm(ProfileFormType::class, $userToEdit);
        $form->setData($userToEdit);
        $form->handleRequest($request);

//        if ($form->isSubmitted() && $form->isValid()) {
//            try {
//                $userManager = $this->get('fos_user.user_manager');
//                $userManager->updateUser($userToEdit);
////            } catch (UniqueConstraintViolationException $exception){
////                return $this->render('@App/errors/updateDatabase.html.twig', [
////                    'error_message' => "Taki użytkownik juz istnieje"
////                ]);
//            } catch (UniqueConstraintViolationException $exception){
//                $form->get('username')->addError(
//                    new FormError('Taki użytkownik juz istnieje'
//                    )
//                );
//            } catch (\Exception $exception){
//                return $this->render('@App/errors/updateDatabase.html.twig', [
//                    'error_message' => $exception->getMessage()
//                ]);
//            }
//        }

//        return $this->render('@App/user/edit.html.twig', [
//            'user' => $userToEdit
//        ]);
//        return $this->render('@App/user/edit.html.twig', [
//            'form' => $form->createView(),
//            'userId' => $userId
//        ]);
        return $this->updateUserData($request, $userToEdit, $userId);

    }

    /**
     * aktualizacja swoich danych
     *
     * @param Request $request
     *
     * @return Response
     */
    public function editOwnDataAction(Request $request): Response
    {
        $user = $this->getUser();

        if (!($user instanceof User)) {
            return new RedirectResponse($this->generateUrl('homepage'));
        }

        return $this->updateUserData($request, $user);
    }

    /**
     * @param Request  $request
     * @param User     $user
     * @param int|null $userId
     *
     * @return Response
     */
    private function updateUserData(Request $request, User $user, int $userId = null): Response
    {
        $form = $this->createForm(ProfileFormType::class, $user);
        $form->setData($user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $userManager = $this->get('fos_user.user_manager');
                $userManager->updateUser($user);
//            } catch (UniqueConstraintViolationException $exception){
//                return $this->render('@App/errors/updateDatabase.html.twig', [
//                    'error_message' => "Taki użytkownik juz istnieje"
//                ]);
            } catch (UniqueConstraintViolationException $exception) {
                $form->get('username')->addError(
                    new FormError('Taki użytkownik juz istnieje'
                    )
                );
            } catch (\Exception $exception) {

                return $this->render('@App/errors/updateDatabase.html.twig', [
                    'error_message' => $exception->getMessage()
                ]);
            }
        }
        $responseData = [
            'form' => $form->createView()
        ];

        if (!empty($userId)) {
            $responseData['userId'] = $userId;
        }

        return $this->render('@App/user/edit.html.twig', $responseData);
    }

    public function listAction(): JsonResponse
    {
        $user = $this->getUser();

//        if (!($user instanceof User)) {
//            return new JsonResponse(['error' => 'zaloguj się ...'],
//                Response::HTTP_UNAUTHORIZED,
//                ['Content-Type' => 'application/api-problem+json']
//            );
//        }

        if (!($user instanceof User)) {
            return (new ApiProblem())
                ->setTitle('Wymagane Logowanie')
                ->setStatus(Response::HTTP_UNAUTHORIZED)
                ->setDetail('Dostęp do tego zasobu jest ograniczony. Musisz byc zalogowanym użytkownikiem i posiadać odpowiednie uprawnienia')
                ->getResponse();
        }


        $users = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->findAll();

        $responseUsers = [];
        $response = (new ApiResponse())
            ->setStatus(Response::HTTP_OK);

        foreach ($users as $userToResponse) {
            $responseUsers[] = [
                'user_name' => $userToResponse->getUsername(),
                'email' => $userToResponse->getEmail(),
                'name' => ($userToResponse->getUserData() instanceof UserData) ? $userToResponse->getUserData()->getUserName() : '',
                'surname' => ($userToResponse->getUserData() instanceof UserData) ? $userToResponse->getUserData()->getUserSurname() : ''
            ];
        }

//        return new JsonResponse(
//            $responseUsers,
//            Response::HTTP_OK,
//            ['Content-Type' => 'application/vnd.api+json']
//        );

        return $response
            ->setData($responseUsers)
            ->getResponse();
    }


    /**
     * @param Request $request
     * @param int     $userId
     *
     * @return Response
     */
    public function editAddonsDataAction(Request $request, int $userId): Response
    {
        $user = $this->getUser();

        if (!($user instanceof User)) {
            return new RedirectResponse($this->generateUrl('homepage'));
        }

        if (!$this->isGranted('ROLE_ADMIN') && $user->getId() != $userId) {
            return $this->render('@App/errors/403.html.twig', [
                'error_message' => 'Brak uprawnień'
            ]);
        }

        $userToEdit = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->find($userId);

        if (!($userToEdit instanceof User)) {
            return $this->render('@App/errors/404.html.twig', [
                'error_message' => 'Nie ma takiego użytkownika'
            ]);
        }

        $addonsData = $userToEdit->getUserData();

        if (!($addonsData instanceof UserData)) {
            $addonsData = new UserData();
        }

//        $form = $this->createForm(UserDataForm::class, $addonsData);
//        $form->setData($addonsData);
//        $form->handleRequest($request);
//
//        if (!$form->isSubmitted()) {
//
//            return $this->render('AppBundle:user:addonsData.html.twig', [
//                'user_data_form' => $form->createView(),
//                'userId' => $userId
//            ]);
        return $this->editAddonsData($request, $userToEdit, $addonsData, $userId);


    }

    private function editAddonsData(Request $request, User $user, UserData $userData, int $userId = null): Response
    {
        $form = $this->createForm(UserDataForm::class, $userData);
        $form->setData($userData);
        $form->handleRequest($request);


        if (!$form->isSubmitted()) {

//            return $this->render('AppBundle:user:addonsData.html.twig', [
//                'user_data_form' => $form->createView(),
//                'userId' => $userId
//            ]);
            $responseData = ['user_data_form' => $form->createView()];

            if (!empty($userId)) {
                $responseData['userId'] = $userId;
            }

            return $this->render('@App/user/addonsData.html.twig', $responseData);
        }

        if ($form->isValid()) {
            $entityManager = $this->getDoctrine()
                ->getManager();
            $entityManager->persist($userData);

            $user->setUserData($userData);
            $entityManager->persist($user);

            $entityManager->flush();
        }

        if (empty($userId)) {
            return new RedirectResponse(
                $this->generateUrl('edit_own_data')
            );
        }

        return new RedirectResponse(
            $this->generateUrl('edit_user', ['userId' => $userId])
        );


    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function editOwnAddonsDataAction(Request $request): Response
    {
        $user = $this->getUser();

        if (!($user instanceof User)) {
            return new RedirectResponse($this->generateUrl('homepage'));
        }


        $addonsData = $user->getUserData();

        if (!($addonsData instanceof UserData)) {
            $addonsData = new UserData();
        }

        return $this->editAddonsData($request, $user, $addonsData);
    }

//    public function getUserAction(int $userId): JsonResponse
//    {
//        $user= $this->getUser();
//
//
//        if (!($user instanceof User)) {
//            return (new ApiProblem())
//                ->setTitle('Wymagane Logowanie')
//                ->setStatus(Response::HTTP_UNAUTHORIZED)
//                ->setDetail('Dostęp do tego zasobu jest ograniczony. Musisz byc zalogowanym użytkownikiem i posiadać odpowiednie uprawnienia')
//                ->getResponse();
//        }
//
//        $userToResponse = $this->getDoctrine()
//            ->getRepository('AppBundle:User')
//            ->find($userId);
//
//        if(!$userToResponse instanceof User) {
//            return (new ApiProblem())
//                ->setTitle('Nie znaleziono uzytkownika')
//                ->setStatus(Response::HTTP_NOT_FOUND)
//                ->setDetail('Nie udało się odnażć użytkownika o wskazanym ID')
//                ->getResponse();
//        }
//
//        $response[] = [
//            'id' => $userToResponse->getId(),
//            'user_name' => $userToResponse->getUsername(),
//            'email' => $userToResponse->getEmail(),
//            'name' => ($userToResponse->getUserData() instanceof  UserData) ? $userToResponse->getUserData()->getUsername(): '' ,
//            'surname' => ($userToResponse->getUserData() instanceof  UserData) ? $userToResponse->getUserData()->getUserSurname(): ''
//        ];
//
//            return (new ApiResponse())
//                ->setData($response)
//                ->setStatus(Response::HTTP_OK)
//                ->getResponse();
//    }

    public function showUserNameAndSurnameAction(int $userId): Response
    {
        $user = $this->getUser();
        $ajax = new AjaxResponse();


        if (!($user instanceof User)) {
            $ajax->alertError('Wymaganie logowanie');

            return new Response($ajax);
        }

        $userToResponse = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->find($userId);

        if (!($userToResponse instanceof User)) {
            $ajax->alertError('Nie znalazlem użytkownika');

            return new Response($ajax);
        }

        $userNameAndSurname =
            (($userToResponse->getUserData() instanceof UserData)
                ? $userToResponse->getUserData()->getUserName()
                : '')
            . ' ' .
            (($userToResponse->getUserData() instanceof UserData)
                ? $userToResponse->getUserData()->getUserSurname()
                : '');

        $ajax->assign('#user_' . $userId, $userNameAndSurname);

        return new Response($ajax);
    }


}
