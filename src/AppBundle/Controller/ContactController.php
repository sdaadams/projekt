<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class ContactController extends Controller
{
    public function indexAction(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('Email', EmailType::class)
            ->add('Temat', TextType::class)
            ->add('Wiadomosc', TextareaType::class)
            ->add('Wyslij', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form->getData();

            $message = \Swift_Message::newInstance()
                ->setSubject($data['Temat'])
                ->setFrom($data['Email'])
                ->setTo('adamtestowyprojekt@gmail.com')
                ->setBody(
                    $form->getData()['Wiadomosc'],
                    'text/plain'
                );

            $this->get('mailer')->send($message);
            echo 'Wiadomość została wysłana';
        }

        return $this->render('@App/default/contact.html.twig', [
            'contact_form' => $form->createView()
        ]);
    }
}
