<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Province;


class ProvinceController extends Controller
{
    /**
     * @return Response
     */
    public function indexAction(): Response
    {

        $user = $this->getUser();

        if (!($user instanceof User)) {
            return new RedirectResponse($this->generateUrl('homepage'));
        }


        $province = $this->getDoctrine()
            ->getRepository('AppBundle:Province')
            ->findAll();

        return $this->render('@App/province/provinces.html.twig', [
            'provinces' => $province
        ]);
    }

    public function showProvinceLakesAction(int $provinceId)
    {
        $province = $this->getDoctrine()
            ->getRepository('AppBundle:Province')
            ->findBy(['province_id' => $provinceId]);

        if (!$province instanceof Province) {
            new \TypeError('Błąd');
        }

        $lakes = $this->getDoctrine()
            ->getRepository('AppBundle:Lakes')
            ->findBy(['province' => $province]);

        return $this->render('@App/lakes/lakesid.html.twig', [
            'province_id' => $lakes
        ]);
    }


}