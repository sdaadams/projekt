<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Lakes;


class LakesController extends Controller
{
    /**
     * @return Response
     */
    public function indexAction(): Response
    {


        $user = $this->getUser();

        if (!($user instanceof User)) {
            return new RedirectResponse($this->generateUrl('homepage'));
        }

        $lakes = $this->getDoctrine()
            ->getRepository('AppBundle:Lakes')
            ->findAll();

        return $this->render('@App/user/lakes.html.twig', [
            'lakes' => $lakes
        ]);
    }


    public function showLakeAction(int $id): Response
    {

        $user = $this->getUser();

        if (!($user instanceof User)) {
            return new RedirectResponse($this->generateUrl('homepage'));
        }


        $lake = $this->getDoctrine()
            ->getRepository('AppBundle:Lakes')
            ->findBy(['id' => $id]);

        if (!$lake instanceof Lakes) {
            new \TypeError('Błąd');
        }

        $images = $this->getDoctrine()
            ->getRepository('AppBundle:Post')
            ->findBy(['lakes' => $lake]);


        return $this->render('@App/lakes/lakeSite.html.twig', [
            'id' => $id, 'lake' => $lake, 'images' => $images
        ]);

    }


}