<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use AppBundle\Entity\User;
use AppBundle\Form\PostType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class PostController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('', array('name' => $name));
    }

    /**
     * @Route("post/nev", name="nev_post")
     * @param Request $request
     *
     * @return Response
     */
    public function createPostAction(Request $request): Response
    {

        $user = $this->getUser();

        if (!($user instanceof User)) {
            return new RedirectResponse($this->generateUrl('homepage'));
        }

        if (!$this->isGranted('ROLE_ADMIN')) {
            return $this->render('@App/errors/403.html.twig', [
                'error_message' => 'Brak uprawnień'
            ]);
        }

        $post = new Post();

        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {

            /** @var UploadedFile $file */
            $file = $post->getImage();
            $fileName = md5(uniqid()) . '.' . $file->guessExtension();

            $file->move(
                $this->getParameter('image_directory'), $fileName
            );

            $post->setImage($fileName);
            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();

            return $this->render('post/show.html.twig', array('post' => $post));
        }

        return $this->render('post/new.html.twig', array('form' => $form->createView()));

    }

    public function showPostAction()
    {
        $post = $this->getDoctrine()->getRepository('AppBundle:Post')->find(1);

        return $this->render('post/show.html.twig', array('post' => $post));
    }
}
