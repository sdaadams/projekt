<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserDataForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user_name', TextType::class, ['attr' => ['maxlength' => 25], 'label' => 'Imie'])
            ->add('user_surname', TextType::class, ['attr' => ['maxlength' => 50], 'label' => 'Nazwisko'])
            ->add('user_cellular', TextType::class, ['attr' => ['maxlength' => 20, 'label' => 'Nr telefonu']]);

        $options = [];
    }

    public function getBlockPrefix()
    {
        return 'app_bundle_user_data_form';
    }
}
