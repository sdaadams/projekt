<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;

/**
 * User
 */
class User extends BaseUser
{
    /**
     * @var integer
     */
    protected $id;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @var \AppBundle\Entity\UserData
     */
    private $user_data;


    /**
     * Set userData
     *
     * @param \AppBundle\Entity\UserData $userData
     *
     * @return User
     */
    public function setUserData(\AppBundle\Entity\UserData $userData = null)
    {
        $this->user_data = $userData;

        return $this;
    }

    /**
     * Get userData
     *
     * @return \AppBundle\Entity\UserData
     */
    public function getUserData()
    {
        return $this->user_data;
    }


}
