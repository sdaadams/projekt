<?php

namespace AppBundle\Entity;

/**
 * Province
 */
class Province
{
    /**
     * @var int
     */
    private $province_id;

    /**
     * @var string
     */
    private $name;


    /**
     * Get provinceId.
     *
     * @return int
     */
    public function getProvinceId()
    {
        return $this->province_id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Province
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
