<?php

namespace AppBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;

class Category
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    private $name;


    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }


//    /**
//     * @ORM\OneToMany(targetEntity="BlogPost", mappedBy="category")
//     */
//    private $blogPosts;

//    public function __construct()
//    {
//        $this->blogPosts = new ArrayCollection();
//    }

//    public function getBlogPosts()
//    {
//        return $this->blogPosts;
//    }


}
