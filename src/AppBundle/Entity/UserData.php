<?php

namespace AppBundle\Entity;

/**
 * UserData
 */
class UserData
{
    /**
     * @var integer
     */
    private $user_data_id;

    /**
     * @var string
     */
    private $user_name;

    /**
     * @var string
     */
    private $user_surname;

    /**
     * @var string
     */
    private $user_cellular;


    /**
     * Get userDataId
     *
     * @return integer
     */
    public function getUserDataId()
    {
        return $this->user_data_id;
    }

    /**
     * Set userName
     *
     * @param string $userName
     *
     * @return UserData
     */
    public function setUserName($userName)
    {
        $this->user_name = $userName;

        return $this;
    }

    /**
     * Get userName
     *
     * @return string
     */
    public function getUserName()
    {
        return $this->user_name;
    }

    /**
     * Set userSurname
     *
     * @param string $userSurname
     *
     * @return UserData
     */
    public function setUserSurname($userSurname)
    {
        $this->user_surname = $userSurname;

        return $this;
    }

    /**
     * Get userSurname
     *
     * @return string
     */
    public function getUserSurname()
    {
        return $this->user_surname;
    }

    /**
     * Set userCellular
     *
     * @param string $userCellular
     *
     * @return UserData
     */
    public function setUserCellular($userCellular)
    {
        $this->user_cellular = $userCellular;

        return $this;
    }

    /**
     * Get userCellular
     *
     * @return string
     */
    public function getUserCellular()
    {
        return $this->user_cellular;
    }
}
